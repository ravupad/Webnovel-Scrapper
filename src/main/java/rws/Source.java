package rws;

import java.util.Scanner;

public interface Source {
	Book selectBook(Scanner scanner);
}
